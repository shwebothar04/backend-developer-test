﻿using eCommerceApi.BusinessLogic.BusinessEntities;
using eCommerceApi.BusinessLogic.Interface;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eCommerceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductBL iProductBL;

        public ProductController(IProductBL _iProductBL)
        {
            iProductBL = _iProductBL;
        }

        // GET: api/<ProductController>
        /// <summary>
        /// Search product by keyword.
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("SearchProduct")]
        public IEnumerable<Product> SearchProduct(string keywords)
        {
            var rtn = iProductBL.SearchProduct(keywords);
            return rtn;
        }
        
    }
}
