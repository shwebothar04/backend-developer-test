﻿using eCommerceApi.BusinessLogic.BusinessEntities;
using eCommerceApi.BusinessLogic.Interface;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace eCommerceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderBL iOrderBL;

        public OrderController(IOrderBL _iOrderBL)
        {
            iOrderBL = _iOrderBL;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PlaceOrder")]
        public string PlaceOrder([FromBody] OrderRequest orderRequest)
        {
            try
            {
                if (orderRequest == null || string.IsNullOrWhiteSpace(orderRequest.OrderID) || orderRequest.ProductList == null || orderRequest.ProductList.Count() == 0)
                {
                    return "Order details are required.";
                }

                var rtn = iOrderBL.PlaceOrder(orderRequest);
                return rtn;
            }
            catch(Exception ex)
            {
                return "Server Error.";
            }
        }
    }
}
