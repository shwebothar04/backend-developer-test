﻿using eCommerceApi.DataAccess.EFCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess.Interface
{
    public interface IProductDA : IEFCoreRepository<T_Product>
    {
        IEnumerable<T_Product> SearchProduct(string keyword);
    }
}
