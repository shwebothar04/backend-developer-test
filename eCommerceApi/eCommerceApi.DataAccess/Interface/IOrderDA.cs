﻿using eCommerceApi.DataAccess.EFCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess.Interface
{
    public interface IOrderDA : IEFCoreRepository<T_Order>
    {
        //IEnumerable<T_Order> PlaceOrder(T_Order entity);
    }
}
