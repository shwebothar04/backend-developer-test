﻿using eCommerceApi.DataAccess.EFCore;
using eCommerceApi.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess
{
    public class ProductDA : EFCoreRepository, IProductDA
    {
        public ProductDA(EFCoreDbContext _db) : base(_db)
        {

        }

        public IEnumerable<T_Product> SearchProduct(string keyword)
        {
            var dbEntities = db.Product.Where(o => o.IsDeleted == false).ToList();
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = keyword.ToLower();
                dbEntities = dbEntities.Where(o => o.Name.ToLower().Contains(keyword) || o.Description.ToLower().Contains(keyword) || o.Category.ToLower().Contains(keyword)).ToList();
            }
            return dbEntities;
        }

        public IEnumerable<T_Product> GetAll()
        {
            var dbEntities = db.Product.Where(o => o.IsDeleted == false).ToList();
            return dbEntities;
        }

        public T_Product Get(int id)
        {
            var entity = db.Product.Find(id);
            return entity;
        }

        public int Add(T_Product entity)
        {
            db.Product.Add(entity);
            db.SaveChanges();

            return entity.ProductId;
        }

        public void Update(T_Product entity)
        {
            var dbObj = db.Product.Find(entity.ProductId);
            if (dbObj != null)
            {
                dbObj.Name = entity.Name;
                dbObj.Description = entity.Description;
                dbObj.Category = entity.Category;
                dbObj.Price = entity.Price;
                db.Product.Update(dbObj);
            }

            //Commit the transaction
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var dbObj = db.Product.Find(id);
            if (dbObj != null)
            {
                dbObj.IsDeleted = true;
                db.Product.Update(dbObj);
            }

            db.SaveChanges();
        }
    }
}
