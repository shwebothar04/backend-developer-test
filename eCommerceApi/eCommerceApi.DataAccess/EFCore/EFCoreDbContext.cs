﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

// http://www.mukeshkumar.net/articles/dotnetcore/crud-operation-in-asp-net-core-web-api-with-entity-framework-core

namespace eCommerceApi.DataAccess.EFCore
{
    public class EFCoreDbContext : DbContext
    {
        private string connectionString;

        //public EFCoreDbContext(DbContextOptions<EFCoreDbContext> options, IConfiguration _configuration) : base(options)
        public EFCoreDbContext(DbContextOptions<EFCoreDbContext> options) : base(options)
        {
            //this.connectionString = _configuration.GetValue<string>("ConnectionStrings:eComConnectionString");
        }

        public virtual DbSet<T_Product> Product { get; set; }
        public virtual DbSet<T_Order> Order { get; set; }
        public virtual DbSet<T_OrderDetail> OrderDetail { get; set; }

        //public virtual DbSet<Post> Post { get; set; }
        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer("Server=DESKTOP-XYZ;Database=BlogDB;UID=sa;PWD=££££££;");
        //            }
        //        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Category>(entity =>
        //    {
        //        entity.Property(e => e.Id).HasColumnName("ID");

        //        entity.Property(e => e.Name)
        //            .HasColumnName("NAME")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Slug)
        //            .HasColumnName("SLUG")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<Post>(entity =>
        //    {
        //        entity.Property(e => e.PostId).HasColumnName("POST_ID");

        //        entity.Property(e => e.CategoryId).HasColumnName("CATEGORY_ID");

        //        entity.Property(e => e.CreatedDate)
        //            .HasColumnName("CREATED_DATE")
        //            .HasColumnType("datetime");

        //        entity.Property(e => e.Description)
        //            .HasColumnName("DESCRIPTION")
        //            .IsUnicode(false);

        //        entity.Property(e => e.Title)
        //            .HasColumnName("TITLE")
        //            .HasMaxLength(2000)
        //            .IsUnicode(false);

        //        entity.HasOne(d => d.Category)
        //            .WithMany(p => p.Post)
        //            .HasForeignKey(d => d.CategoryId)
        //            .HasConstraintName("FK__Post__CATEGORY_I__1273C1CD");
        //    });
        //}
    }
}
