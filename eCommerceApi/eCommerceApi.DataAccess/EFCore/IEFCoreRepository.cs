﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess.EFCore
{
    public interface IEFCoreRepository<TEntity>
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(int id);
        int Add(TEntity model);
        void Update(TEntity model);
        void Delete(int model);

        //Task<IEnumerable<TEntity>> GetAll();
        //Task<TEntity> Get(int id);
        //Task<int> Add(TEntity entity);
        //Task Update(TEntity entity);
        //Task Delete(int id);
    }
}
