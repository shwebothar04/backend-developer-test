﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess.EFCore
{
    [Table("T_Order")]
    public class T_Order
    {
        [Key]
        public int OrderId { get; set; }
        public string OrderRefId { get; set; }
        public decimal TotalPrice { get; set; }
        public string Status { get; set; }
        public bool IsDeleted { get; set; }
    }
}
