﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace eCommerceApi.DataAccess.EFCore
{
    //public class EFCoreRepository<TEntity> : IEFCoreRepository<TEntity>
    public class EFCoreRepository
    {
        protected EFCoreDbContext db;
        protected string languageID = "en-US";
        public EFCoreRepository(EFCoreDbContext _db)
        {
            db = _db;
            languageID = Thread.CurrentThread.CurrentCulture.Name;
        }
    }
}
