﻿using eCommerceApi.DataAccess.EFCore;
using eCommerceApi.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess
{
    public class OrderDetailDA : EFCoreRepository, IOrderDetailDA
    {
        public OrderDetailDA(EFCoreDbContext _db) : base(_db)
        {

        }

        public IEnumerable<T_OrderDetail> GetAll()
        {
            var dbEntities = db.OrderDetail.Where(o => o.IsDeleted == false).ToList();
            return dbEntities;
        }

        public T_OrderDetail Get(int id)
        {
            var entity = db.OrderDetail.Find(id);
            return entity;
        }

        public int Add(T_OrderDetail entity)
        {
            db.OrderDetail.Add(entity);
            db.SaveChanges();

            return entity.OrderDetailId;
        }

        public void Update(T_OrderDetail entity)
        {
            //var dbObj = db.OrderDetail.Find(entity.OrderDetailId);
            //if (dbObj != null)
            //{
            //    db.OrderDetail.Update(dbObj);
            //}

            ////Commit the transaction
            //db.SaveChanges();
        }

        public void Delete(int id)
        {
            var dbObj = db.OrderDetail.Find(id);
            if (dbObj != null)
            {
                dbObj.IsDeleted = true;
                db.OrderDetail.Update(dbObj);
            }

            db.SaveChanges();
        }
    }
}
