﻿using eCommerceApi.DataAccess.EFCore;
using eCommerceApi.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.DataAccess
{
    public class OrderDA : EFCoreRepository, IOrderDA
    {
        public OrderDA(EFCoreDbContext _db) : base(_db)
        {

        }

        public IEnumerable<T_Order> GetAll()
        {
            var dbEntities = db.Order.Where(o => o.IsDeleted == false).ToList();
            return dbEntities;
        }

        public T_Order Get(int id)
        {
            var entity = db.Order.Find(id);
            return entity;
        }

        public int Add(T_Order entity)
        {
            db.Order.Add(entity);
            db.SaveChanges();

            return entity.OrderId;
        }

        public void Update(T_Order entity)
        {
            //var dbObj = db.Order.Find(entity.OrderId);
            //if (dbObj != null)
            //{
                
            //    db.Order.Update(dbObj);
            //}

            ////Commit the transaction
            //db.SaveChanges();
        }

        public void Delete(int id)
        {
            var dbObj = db.Order.Find(id);
            if (dbObj != null)
            {
                dbObj.IsDeleted = true;
                db.Order.Update(dbObj);
            }

            db.SaveChanges();
        }
    }
}
