﻿using Microsoft.EntityFrameworkCore;
using eCommerceApi.DataAccess.EFCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using eCommerceApi.DataAccess.Interface;

namespace eCommerceApi.DataAccess
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// configure Dependency Injection for DataAccess class for BusinessLogic layer
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterDAServices(this IServiceCollection services, IConfiguration _configuration)
        {
            //services.AddDbContext<EFCoreDbContext>(options => options.UseSqlServer(_configuration["ConnectionStrings:eComConnectionString"]));
            services.AddDbContext<EFCoreDbContext>(options => options.UseSqlServer("name=ConnectionStrings:eComConnectionString"));

            services.AddTransient<IProductDA, ProductDA>();
            services.AddTransient<IOrderDA, OrderDA>();
            services.AddTransient<IOrderDetailDA, OrderDetailDA>();

            return services;
        }
    }
}