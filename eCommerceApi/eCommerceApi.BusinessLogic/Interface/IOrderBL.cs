﻿using eCommerceApi.BusinessLogic.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.BusinessLogic.Interface
{
    public interface IOrderBL
    {
        string PlaceOrder(OrderRequest orderRequest);
    }
}
