﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.BusinessLogic.BusinessEntities
{
    public class OrderDetailRequest
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int ProductQuantity { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
