﻿using eCommerceApi.BusinessLogic.BusinessEntities;
using eCommerceApi.BusinessLogic.Interface;
using eCommerceApi.DataAccess.EFCore;
using eCommerceApi.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.BusinessLogic
{
    public class ProductBL : IProductBL
    {
        IProductDA iProductDA;
        public ProductBL(IProductDA _iProductDA)
        {
            //this.configuration = _configuration;
            this.iProductDA = _iProductDA;
        }

        public IEnumerable<Product> SearchProduct(string keyword)
        {
            var dbProducts = iProductDA.SearchProduct(keyword);
            return GetModelList(dbProducts);
        }

        private List<Product> GetModelList(IEnumerable<T_Product> entitylist)
        {
            var dbresults = new List<Product>();

            int rowIndex = 1;
            Product product;
            foreach (var item in entitylist)
            {
                product = GetModel(item);
                product.RowIndex = rowIndex;
                dbresults.Add(product);
                rowIndex = rowIndex + 1;
            }

            return dbresults;
        }

        private Product GetModel(T_Product entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new Product
            {
                ProductId = entity.ProductId,
                Name = entity.Name,
                Description = entity.Description,
                Category = entity.Category,
                Price = entity.Price
            };
        }
    }
}
