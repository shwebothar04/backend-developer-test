﻿using eCommerceApi.BusinessLogic.Interface;
using eCommerceApi.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerceApi.BusinessLogic
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterBLServices(this IServiceCollection services, IConfiguration _configuration)
        {
            // configure Dependency Injection for DataAccess class for BusinessLogic layer
            services.RegisterDAServices(_configuration);

            services.AddTransient<IProductBL, ProductBL>();
            services.AddTransient<IOrderBL, OrderBL>();

            return services;
        }
    }
}