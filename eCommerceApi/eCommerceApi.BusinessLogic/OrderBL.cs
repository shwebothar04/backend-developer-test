﻿using eCommerceApi.BusinessLogic.BusinessEntities;
using eCommerceApi.BusinessLogic.Interface;
using eCommerceApi.DataAccess.EFCore;
using eCommerceApi.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eCommerceApi.BusinessLogic
{
    public class OrderBL : IOrderBL
    {
        IOrderDA iOrderDA;
        IOrderDetailDA iOrderDetailDA;

        public OrderBL(IOrderDA _iOrderDA, IOrderDetailDA _iOrderDetailDA)
        {
            //this.configuration = _configuration;
            this.iOrderDA = _iOrderDA;
            this.iOrderDetailDA = _iOrderDetailDA;
        }

        public string PlaceOrder(OrderRequest orderRequest)
        {
            try
            {
                var order = GetOrderEntity(orderRequest);
                var orderId = iOrderDA.Add(order);
                if (orderId > 0)
                {
                    foreach (var product in orderRequest.ProductList)
                    {
                        var orderDetail = new T_OrderDetail
                        {
                            OrderId = orderId,
                            ProductId = product.ProductId,
                            ProductQuantity = product.ProductQuantity,
                            UnitPrice = product.UnitPrice
                        };
                        iOrderDetailDA.Add(orderDetail);
                    }
                }

                return "Order success." + orderRequest.OrderID;
            }
            catch(Exception ex)
            {
                return "Order failed.";
            }
        }

        private T_Order GetOrderEntity(OrderRequest model)
        {
            if (model == null)
            {
                return null;
            }

            return new T_Order
            {
                OrderRefId = model.OrderID,
                TotalPrice = model.TotalPrice,
                Status = model.Status
            };
        }

    }
}
